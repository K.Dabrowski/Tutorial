#include <stdio.h>
#define VER 186
#define TRC 187
#define BRC 188
#define BLC 200
#define TLC 201
#define HOR 205
#define SPA 32

#define QUA 65

int main()
{
    register int cnt;
    //Display Top Right Corner, Line and Top Left Corner
    printf("%c", TLC);
    for(cnt = 0;cnt < QUA; cnt++)
        printf("%c", HOR);
    printf("%c", TRC);

    //Display Right Vertical Line
    printf("\n");
    printf("%c", VER);
    for(cnt = 0;cnt < QUA; cnt++)
        printf("%c", SPA);
    printf("%c", VER);


    //Display Bottom Right Corner, Line and Bottom Left Corner
    printf("\n");
    printf("%c", BLC);
    for(cnt = 0;cnt < QUA; cnt++)
        printf("%c", HOR);
    printf("%c", BRC);

}
