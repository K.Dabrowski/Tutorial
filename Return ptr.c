#include <stdio.h>
#include <stdlib.h>
#define  MAX 5

//Declaration of function with pointers parameters
int* add_arrays(int ar1[], int s1, int ar2[], int s2)
{
    int cnt, sum = 0;
    int ar3[s1];
    int* ptr = ar3;
    for (cnt = 0; cnt < MAX; cnt++)
    {
        sum=ar1[cnt]+ar2[cnt];
        ar3[cnt] = sum;
    }
    return ptr;
}
//You want to return values under pointers and increment your pointer
int main()
{
    int licz;
    // Initialization of arrays
    int ar1[MAX] = {20, 4, 6, 8, 1};
    int ar2[MAX] = {19, 18, 17, 16, 69};
    puts("\t\tAr1\t\tAr2\t\tAr3");
    puts("\n======================================================");
    printf("======================================================");
    for(licz = 0; licz < MAX; licz++)
        printf("\nElement %d:\t%d\t\t%d\t\t%d", licz, ar1[licz], ar2[licz], *(add_arrays(ar1, MAX, ar2, MAX)+licz));
    puts("\n======================================================");
    printf("======================================================");
    printf("\nSize of int is: %d", sizeof(int));
    return 0;
}
