#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#define LEN 5

int *ptr[LEN];

//Functions declarations
void get_nbr(int *p[], int n);

void sort(int *p[], int n, int sort_type);

void print(int *p[], int n, int sort_type);

int asc(int nb1, int nb2);

int des(int nb1, int nb2);

int main()
{
    int sort_type, cnt;
    //Load data from keyboard
    get_nbr(ptr, LEN);

    puts("Before sorting:");
    //Print before sorting
    for(cnt = 0; cnt < LEN; cnt++)
        {
            //Go through pointer array
            printf("%d\n", *ptr[cnt]);
        }

    while(sort_type != 0 && sort_type != 1)
    {
        puts("Type 0 for descending order or 1 for ascending order.");
        scanf("%d", &sort_type);
    }
    sort(ptr, LEN, sort_type);

    puts("After sorting:");
    print(ptr, LEN, sort_type);
    return 0;
}

void get_nbr(int *p[], int n)
{
    int cnt;
    for(cnt = 0; cnt < n; cnt++)
    {
        //Memory reservation for each int pointer
        p[cnt] = (int *)malloc (sizeof(int));
        puts("Type a decimal:");
        scanf("%d", p[cnt]);
    }
}

void sort(int *p[], int n, int sort_type)
{
    int a,b;
    int *tmp;
    //Pointer to function declaration
    int (*cmp)(int nb1, int nb2);
    //F_ptr initialization
    (cmp = (sort_type) ? asc : des);
    //Compare adjacent numbers
    for(a = 1; a < n; a++)
         for(b = 0; b < n-1; b++)
        {
            //Switch their places if needed
            //Function call
           if(cmp(*p[b], *p[b+1]) == 1)
            {
               tmp = p[b];
               p[b] = p[b+1];
               p[b+1] = tmp;
            }
        }
}

int asc(int nb1, int nb2)
{
    int res;
    res = nb1 > nb2 ? 1 : 0;
        return res;
}

int des(int nb1, int nb2)
{
     int res;
     res = nb1 < nb2 ? 1 : 0;
        return res;
}

void print(int *p[], int n, int sort_type)
{
    int c;
    if (sort_type == 1)
        puts("You've chosen ascending order:");
    else
        puts("You've chosen descending order:");
     //Go through pointer array
    for (c = 0; c < n; c++)
        printf("%d\n", *p[c]);
}




