#include <stdio.h>
#define MAX 5
int main()
{
    int array[MAX];
    int idx = -1,nbr = 0;
    int tb = 0, tl = 0, tmp;
    printf("this program wants you to type in %d values\n", MAX);
    puts("Type 0 to end program");

        do
    {
        printf("Type number %d from %d:\n", idx + 2, MAX);
        scanf("%d", &tmp);
        array[++idx] = tmp;
     } while ( idx < (MAX-1) && tmp != 0 );
        nbr = idx;
        //Search for extreme values
        //Setting initial values to very low and very big
        tb = -32000;
        tl = 32000;
        //idx <= nbr checks if an element is within the array;

    for(idx = 0;  idx <= nbr && array[idx] != 0; idx++)
        {
            if(array[idx] > tb)
                tb = array[idx];
            if(array [idx] < tl)
                tl = array[idx];
        }

    printf("The biggest value is: %d\n", tb);
    printf("The lowest value is: %d\n", tl);
}
